import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;

public class Tetris extends JPanel {
	
	private static final long serialVersionUID = 1L;

	private JFrame frame = new JFrame("Tetris");
    
    private final Point[][][] Pieces = {
            // I-Piece
       {
           { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(3, 1) },
           { new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(1, 3) },
           { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(3, 1) },
           { new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(1, 3) }
        },
            
            // J-Piece
        {
           { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(2, 0) },
           { new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 2) },
           { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(0, 2) },
           { new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 0) }
        },
            
            // L-Piece
        {
            { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(2, 2) },
            { new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 2) },
            { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(0, 0) },
            { new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 0) }
        },
            
            // O-Piece
        {
            { new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) },
            { new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) },
            { new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) },
            { new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) }
        },
            
            // S-Piece
        {
            { new Point(1, 0), new Point(2, 0), new Point(0, 1), new Point(1, 1) },
            { new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2) },
            { new Point(1, 0), new Point(2, 0), new Point(0, 1), new Point(1, 1) },
            { new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2) }
        },
            
            // T-Piece
        {
            { new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(2, 1) },
            { new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2) },
            { new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(1, 2) },
            { new Point(1, 0), new Point(1, 1), new Point(2, 1), new Point(1, 2) }
        },
            
            // Z-Piece
        {
            { new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(2, 1) },
            { new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2) },
            { new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(2, 1) },
            { new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2) }
        }
    };
    
    private Color[] pieceColors = {
        Color.red, Color.orange, Color.yellow, Color.cyan, Color.blue, Color.green, Color.magenta
        //random colors
        //getColor(), getColor(), getColor(), getColor(), getColor(), getColor(),getColor()
    };
    
    //private Color randomColor;
    private Point startPoint;
    private int currentPiece;
    private int rotation;
    private int wellWidth = 17;
    private int wellHeight = 25;
    private int tempo = 1000;
    private int cellSize = 30;
    private int fwidth = wellWidth*cellSize+250;
    private int fheight = wellHeight*cellSize+40;
    
    private boolean isOn = false;
    
    private ArrayList<Integer> nextPieces = new ArrayList<Integer>();

    private long score;
    private Color[][] well;
    
    /*/random piece color generation
    private Color getColor(){
        Random rand = new Random();
        int red = rand.nextInt((255-100)+1)+100;
        int green = rand.nextInt((255-100)+1)+100;
        int blue = rand.nextInt((255-100)+1)+100;
       
        randomColor = new Color(red, green, blue);
        return randomColor;
    }*/
    // Creates a well and drops a first piece
    private void init() {
        well = new Color[wellWidth][wellHeight];
        for (int i = 0; i < wellWidth; i++) {
            for (int j = 0; j < (wellHeight); j++) {
                if (i == 0 || i == (wellWidth-1) || j == (wellHeight-1)) {
                    well[i][j] = Color.GRAY;
                } else {
                    well[i][j] = Color.BLACK;
                }
            }
        }
    }
    // Puts a random piece in a dropping position
    public void newPiece() {
        startPoint = new Point(7, 0);
        if(well[8][1] == Color.BLACK){
            rotation = 0;
            if (nextPieces.isEmpty()) {
                Collections.addAll(nextPieces, 0, 1, 2, 3, 4, 5, 6);
                Collections.shuffle(nextPieces);
            }
            currentPiece = nextPieces.get(0);
            nextPieces.remove(0);
        } else {
            JFrame lost = new JFrame();
            lost.setSize(400,250);
            lost.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
            lost.setVisible(false);
            int o = JOptionPane.showConfirmDialog(lost,"You lost, wanna play again?");
            if(o==JOptionPane.YES_OPTION){
                for (int i = 0; i < wellWidth; i++) {
                    for (int j = 0; j < (wellHeight); j++) {
                        if (i == 0 || i == (wellWidth-1) || j == (wellHeight-1)) {
                            well[i][j] = Color.GRAY;
                        } else {
                            well[i][j] = Color.BLACK;
                        }
                    }
                }
                
                isOn = false;
                score = 0;
                tempo = 1000;
            } else if (o==JOptionPane.NO_OPTION){
                System.exit(0);
            }
        }
    }
    // Collision test
    private boolean collision(int x, int y, int rotation) {
        for (Point p : Pieces[currentPiece][rotation]) {
            if (well[p.x + x][p.y + y] != Color.BLACK) {
                return true;
            }
        }
        return false;
    }
    // Rotates the piece
    public void rotate(int i) {
        int newRotation = (rotation + i) % 4;
        if (newRotation < 0) {
            newRotation = 3;
        }
        if (!collision(startPoint.x, startPoint.y, newRotation)) {
            rotation = newRotation;
        }
        repaint();
    }
    // Moves the piece left or right
    public void move(int i) {
        if (!collision(startPoint.x + i, startPoint.y, rotation)) {
            startPoint.x += i; 
        }
        repaint();
    }
    // Drops the piece down one line or fixes it to the well
    public void lowerPiece() {
        if (!collision(startPoint.x, startPoint.y + 1, rotation)) {
            startPoint.y += 1;
        } else {
            fixToWell();
        }   
        repaint();
    }
    // Fixes pieces as part of the well for collision detection
    public void fixToWell() {
        for (Point p : Pieces[currentPiece][rotation]) {
            well[startPoint.x + p.x][startPoint.y + p.y] = pieceColors[currentPiece];
        }
        tempo=tempo*39/40;
        clearRows();
        newPiece();
    }
    public void deleteRow(int row) {
        for (int j = row-1; j > 0; j--) {
            for (int i = 1; i < (wellWidth-1); i++) {
                well[i][j+1] = well[i][j];
            }
        }
    }
    
    // Clears completed rows and adds coresponding points amount
    public void clearRows() {
        boolean gap;
        int rowClear = 0;
        
        for (int j = (wellHeight-2); j > 0; j--) {
            gap = false;
            for (int i = 1; i < wellWidth; i++) {
                if (well[i][j] == Color.BLACK) {
                    gap = true;
                    break;
                }
            }
            if (!gap) {
                deleteRow(j);
                j += 1;
                rowClear += 1;
            }
        }
        switch (rowClear) {
            case 1:
                score += 200*(1000/tempo);
                break;
            case 2:
                score += 500*(1000/tempo);
                break;
            case 3:
                score += 1000*(1000/tempo);
                break;
            case 4:
                score += 2000*(1000/tempo);
                break;
        }
    }
    // Draws the falling piece
    private void drawPiece(Graphics g) {        
        g.setColor(pieceColors[currentPiece]);
        for (Point p : Pieces[currentPiece][rotation]) {
            g.fillRect((p.x + startPoint.x) * cellSize, 
                       (p.y + startPoint.y) * cellSize, 
                       cellSize-1, cellSize-1);
        }
    }
    @Override 
    public void paintComponent(Graphics g)
    {
        g.fillRect(0, 0, fwidth, fheight);
        for (int i = 0; i < wellWidth; i++) {
            for (int j = 0; j < wellHeight; j++) {
                g.setColor(well[i][j]);
                g.fillRect(cellSize*i, cellSize*j, cellSize-1, cellSize-1);
            }
        }
        
        g.setColor(Color.WHITE);
        g.drawString("Score: " + score, wellWidth*cellSize+15, 20);
        
        g.drawString("Press Enter to start/unpause", wellWidth*cellSize+15, 80);
        g.drawString("Hold ESC to pause", wellWidth*cellSize+15, 110);
        g.drawString("Hold R to restart the game", wellWidth*cellSize+15, 140);
        
        g.drawString("Movement keys: " , wellWidth*cellSize+15, 200);
        g.drawString("Left arrow - left " , wellWidth*cellSize+15, 260);
        g.drawString("Right arrow - right " , wellWidth*cellSize+15, 290);
        g.drawString("Shift/Up arrow - rotate clockwise " , wellWidth*cellSize+15, 320);
        g.drawString("Control - rotate counterclockwise " , wellWidth*cellSize+15, 350);
        g.drawString("Down arrow - speed up " , wellWidth*cellSize+15, 380);
        
        g.drawString("Disclaimer:", wellWidth*cellSize+15, wellHeight*cellSize-160);
        g.drawString("Every time the piece sticks to the well,", wellWidth*cellSize+15, wellHeight*cellSize-130);
        g.drawString("the game speeds up a little.", wellWidth*cellSize+15, wellHeight*cellSize-100);
        g.drawString("The higher the speed,", wellWidth*cellSize+15, wellHeight*cellSize-70);
        g.drawString("the more points you earn.", wellWidth*cellSize+15, wellHeight*cellSize-40);
        g.drawString("Arrow speed boost also counts.", wellWidth*cellSize+15, wellHeight*cellSize-10);
        
        drawPiece(g);
    }
    public void game(){
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(fwidth, fheight);
        frame.setVisible(true);
        
        final Tetris game = new Tetris();
        game.init();
        game.newPiece();
        frame.add(game);
                       
        // Keyboard controls
        frame.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                case KeyEvent.VK_SHIFT:
                    game.rotate(-1);
                    break;
                case KeyEvent.VK_UP:
                    game.rotate(-1);
                    break;
                case KeyEvent.VK_CONTROL:
                    game.rotate(+1);
                    break;
                case KeyEvent.VK_LEFT:
                    game.move(-1);
                    break;
                case KeyEvent.VK_RIGHT:
                    game.move(+1);
                    break;
                case KeyEvent.VK_ESCAPE:{
                    isOn = false;
                    JFrame pause = new JFrame();
                    int o = JOptionPane.showConfirmDialog(pause,"Game paused. Do you want to continue?");
                    if(o==JOptionPane.YES_OPTION){
                        isOn = true;
                    } else if (o==JOptionPane.NO_OPTION){
                        System.exit(0);
                    }
                    break;
                }
                case KeyEvent.VK_R:
                    game.score = 0;
                    game.tempo = 1000;
                    game.init();
                    game.startPoint.y = 0;
                    game.startPoint.x = 8;
                    break;
                case KeyEvent.VK_ENTER:
                    isOn = true;
                    break;
                case KeyEvent.VK_DOWN:
                    game.lowerPiece();
                    game.score += (int)(5000/tempo);
                    break;
                } 
            }
            public void keyReleased(KeyEvent e) {
            }
        });
        // Make the falling piece drop at rate depending on tempo(delay)
        new Thread() {
            @Override public void run() {
                while (true) {
                    try {
                        if(isOn){
                        Thread.sleep(game.tempo/2);    
                        game.lowerPiece();
                        game.score += (int)(1000/game.tempo);
                        Thread.sleep(game.tempo/2);
                    }else{
                        Thread.sleep(10);
                    }
                    repaint();
                    } catch ( InterruptedException e ) {}
                }
            }
        }.start();
    }
    public static void main(String[] args) {
        Tetris tetris = new Tetris();
        tetris.game();
    }
}